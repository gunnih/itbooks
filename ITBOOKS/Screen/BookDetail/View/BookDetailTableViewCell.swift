//
//  BookDetailTableViewCell.swift
//  ITBOOKS
//
//  Created by Sanggeon Park on 2021/05/10.
//

import UIKit

class BookDetailTableViewCell: UITableViewCell {
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var bookImageView: UIImageView!
    @IBOutlet weak var subtitle: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var isbn13: UILabel!
    @IBOutlet weak var authors: UILabel!
    @IBOutlet weak var publisher: UILabel!
    @IBOutlet weak var language: UILabel!
    @IBOutlet weak var isbn10: UILabel!
    @IBOutlet weak var pages: UILabel!
    @IBOutlet weak var year: UILabel!
    @IBOutlet weak var rating: UILabel!
    @IBOutlet weak var desc: UILabel!
    
    func updateFrom(book: BookData) {
        title.text = book.title
        subtitle.text = book.subtitle
        price.text = book.price
        isbn13.text = book.isbn13
        authors.text = book.authors
        publisher.text = book.publisher
        language.text = book.language
        isbn10.text = book.isbn10
        pages.text = book.pages
        year.text = book.year
        rating.text = book.rating
        desc.text = book.desc
    }
    
    func updateBookImage(image: UIImage?) {
        bookImageView.image = image
    }
}
