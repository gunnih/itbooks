//
//  BookDetailTableViewController.swift
//  ITBOOKS
//
//  Created by Sanggeon Park on 2021/05/10.
//

import UIKit

class BookDetailTableViewController: UITableViewController {
    private let serviceProvider: ServiceProvider
    private var bookData: BookData
    
    enum CellIndex: Int, CaseIterable {
        case info
        case url
        case note
    }

    init(bookData: BookData, serviceProvider: ServiceProvider) {
        self.serviceProvider = serviceProvider
        self.bookData = bookData
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = bookData.title
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "URLTableViewCell")

        tableView.register(UINib(nibName: "BookDetailTableViewCell", bundle: nil),
                           forCellReuseIdentifier: "BookDetailTableViewCell")
        tableView.register(UINib(nibName: "UserNoteTableViewCell", bundle: nil),
                           forCellReuseIdentifier: "UserNoteTableViewCell")

        serviceProvider.bookDetailWith(isbn13: bookData.isbn13) { [weak self] (book, error) in
            guard let book = book, error == nil else {
                // TODO: handle error
                return
            }
            guard let strongSelf = self else {
                return
            }
            strongSelf.bookData = book
            strongSelf.tableView.reloadData()
        }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return CellIndex.allCases.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch CellIndex(rawValue: indexPath.row) {
        case .info:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "BookDetailTableViewCell",
                                                                 for: indexPath) as? BookDetailTableViewCell else {
                fatalError("Cannot dequeue BookDetailTableViewCell")
            }
            cell.updateFrom(book: bookData)
            serviceProvider.imageDataFor(book: bookData) { isbn13, imageData, error in
                guard error == nil, let data = imageData, let image = UIImage(data: data) else {
                    return
                }
                cell.updateBookImage(image: image)
            }
            return cell
        case .url:
            let cell = tableView.dequeueReusableCell(withIdentifier: "URLTableViewCell", for: indexPath)
            cell.textLabel?.text = bookData.url
            return cell
        default:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "UserNoteTableViewCell",
                                                                 for: indexPath) as? UserNoteTableViewCell else {
                fatalError("Cannot dequeue BookDetailTableViewCell")
            }
            cell.note.text = bookData.note
            cell.note.delegate = self
            return cell
        }
    }


    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        // TODO: use dynamic height
        switch CellIndex(rawValue: indexPath.row) {
        case .info:
            return 750
        default:
            return 60
        }
    }
    
    // MARK: - Table view delegate

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        switch CellIndex(rawValue: indexPath.row) {
        case .url:
            guard let url = URL(string: bookData.url) else {
              return
            }

            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
            break;
        default:
            break;
        }
    }
    
    // MARK: - scroll view delegate
    override func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        let noteCellIndexPath = IndexPath(row: CellIndex.note.rawValue, section: 0)
        if let cell = tableView.cellForRow(at: noteCellIndexPath) as? UserNoteTableViewCell,
           cell.note.isFirstResponder {
            updateNoteFromTextFieldAndResign(textfield: cell.note)
        }
    }
    
    private func updateNoteFromTextFieldAndResign(textfield: UITextField) {
        bookData.note = textfield.text
        serviceProvider.addNote(textfield.text ?? "", book: bookData) { error in
            // TODO: handle error
        }
        textfield.resignFirstResponder()
    }
}

extension BookDetailTableViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        updateNoteFromTextFieldAndResign(textfield: textField)
        return false
    }
}
