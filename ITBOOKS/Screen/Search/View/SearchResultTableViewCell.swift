//
//  SearchResultTableViewCell.swift
//  ITBOOKS
//
//  Created by Sanggeon Park on 2021/05/10.
//

import UIKit

class SearchResultTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var isbnLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var urlLabel: UILabel!
    
    @IBOutlet weak var bookImageView: UIImageView!
    
    private var isbn13: String?

    override func prepareForReuse() {
        titleLabel.text = nil
        subtitleLabel.text = nil
        isbnLabel.text = nil
        priceLabel.text = nil
        urlLabel.text = nil
        bookImageView.image = nil
    }
    
    func updateFrom(book: BookData) {
        titleLabel.text = book.title
        subtitleLabel.text = book.subtitle
        isbnLabel.text = book.isbn13
        priceLabel.text = book.price
        urlLabel.text = book.url

        isbn13 = book.isbn13
    }
    
    func updateBookImage(image: UIImage?, isbn13: String) {
        guard self.isbn13 == isbn13 else {
            bookImageView.image = nil
            return
        }
        bookImageView.image = image
    }

}
