//
//  SearchTableViewController.swift
//  ITBOOKS
//
//  Created by Sanggeon Park on 2021/05/10.
//

import UIKit

class SearchTableViewController: UITableViewController {
    private let serviceProvider = ServiceProvider()

    private let searchResultTableViewController = SearchResultTableViewController()
    
    private lazy var searchController = {
        return UISearchController(searchResultsController: searchResultTableViewController)
    }()
    
    private var currentKeyword: String = ""
    private var results = [BookData]()
    private var searchKeyword = [String]()
    private var currentPage = 1

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = ""
        tableView.tableHeaderView = searchController.searchBar
        self.definesPresentationContext = true
        searchController.searchResultsUpdater = self
        searchController.delegate = self

        searchResultTableViewController.tableView.dataSource = self
        searchResultTableViewController.tableView.delegate = self
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "KeywordTableViewCell")
        searchResultTableViewController.tableView.register(UINib(nibName: "SearchResultTableViewCell", bundle: nil),
                                                           forCellReuseIdentifier: "SearchResultTableViewCell")
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let isSearchResult = (tableView == searchResultTableViewController.tableView)
        return isSearchResult ? results.count : 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let isSearchResult = (tableView == searchResultTableViewController.tableView)
        var cell: UITableViewCell
        if isSearchResult {
            if indexPath.row == results.count - 1 { // last cell
                loadMoreBooks()
            }
            guard let resultCell = tableView.dequeueReusableCell(withIdentifier: "SearchResultTableViewCell",
                                                                 for: indexPath) as? SearchResultTableViewCell else {
                fatalError("Cannot dequeue SearchResultTableViewCell")
            }
            let book = results[indexPath.row]
            resultCell.updateFrom(book: book)
            cell = resultCell
        } else {
            let keywordCell = tableView.dequeueReusableCell(withIdentifier: "KeywordTableViewCell",
                                                            for: indexPath)
            keywordCell.textLabel?.text = ""
            cell = keywordCell
        }
        return cell
    }
    
    // MARK: - Table view delegate

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let isSearchResult = (tableView == searchResultTableViewController.tableView)
        guard isSearchResult else {
            // TODO: search with selected keyword
            return
        }
        let detailViewController = BookDetailTableViewController(bookData: results[indexPath.row],
                                                                 serviceProvider: serviceProvider)
        navigationController?.pushViewController(detailViewController, animated: true)
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let resultCell = cell as? SearchResultTableViewCell else {
            return
        }
        let book = results[indexPath.row]
        serviceProvider.imageDataFor(book: book) { isbn13, imageData, error in
            guard error == nil, let data = imageData, let image = UIImage(data: data) else {
                return
            }
            resultCell.updateBookImage(image: image, isbn13: isbn13)
        }
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        // TODO: use dynamic height
        return 133.0
    }
    
    // MARK: - scroll view delegate
    override func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        let isSearchResult = (scrollView == searchResultTableViewController.tableView)
        
        if isSearchResult, searchController.searchBar.isFirstResponder {
            searchController.searchBar.resignFirstResponder()
        }
    }
}

extension SearchTableViewController: UISearchControllerDelegate {
    func didDismissSearchController(_ searchController: UISearchController) {
        results = []
        searchResultTableViewController.tableView.reloadData()
    }
}

extension SearchTableViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let searchKeyword = searchController.searchBar.text ?? ""
        guard searchKeyword != currentKeyword else {
            return
        }

        if searchKeyword.count > 0 {
            serviceProvider.booksFor(searchKeyword) { [weak self] (page, books, error) in
                // TODO: handle error
                guard let resultList = books,
                      error == nil,
                      self?.currentKeyword == searchKeyword else {
                    return
                }
                self?.currentPage = page ?? 0
                self?.results = resultList
                self?.searchResultTableViewController.tableView.reloadData()
            }
        } else {
            results = []
            searchResultTableViewController.tableView.reloadData()
        }
        currentPage = 0
        currentKeyword = searchKeyword
    }

    private func loadMoreBooks() {
        serviceProvider.booksFor(currentKeyword, page: currentPage + 1) { [weak self] (page, books, error) in
            guard let strongSelf = self else {
                return
            }
            // TODO: handle error
            guard error == nil,
                  let resultList = books,
                  resultList.count > 0,
                  let page = page,
                  page > strongSelf.currentPage else {
                return
            }
            strongSelf.currentPage = page

            let startIndex = strongSelf.results.count
            let endIndex = startIndex + resultList.count - 1
            var newIndexes = [IndexPath]()
            for index in startIndex...endIndex {
                newIndexes.append(IndexPath(row: index, section: 0))
            }
            strongSelf.results.append(contentsOf: resultList)
            strongSelf.searchResultTableViewController.tableView.beginUpdates()
            strongSelf.searchResultTableViewController.tableView.insertRows(at: newIndexes, with: .none)
            strongSelf.searchResultTableViewController.tableView.endUpdates()
            strongSelf.loadMoreBooks()
        }
    }
}
