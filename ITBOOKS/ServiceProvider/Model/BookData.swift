//
//  BookData.swift
//  ITBOOKS
//
//  Created by Sanggeon Park on 2021/05/09.
//

import Foundation

struct BookData {
    let title: String
    let subtitle: String
    let price: String
    let image: String
    let isbn13: String
    let url: String

    // MARK: Optional properties
    let authors: String?
    let publisher: String?
    let isbn10: String?
    let pages: String?
    let year: String?
    let rating: String?
    let desc: String?
    let language: String?
    let PDFDataArray: [SectionPDFData]?
    var note: String?
}
