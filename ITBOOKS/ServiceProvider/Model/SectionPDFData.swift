//
//  SectionPDFData.swift
//  ITBOOKS
//
//  Created by Sanggeon Park on 2021/05/09.
//

import Foundation

struct SectionPDFData {
    let isbn13: String
    let url: String
    let title: String
}
