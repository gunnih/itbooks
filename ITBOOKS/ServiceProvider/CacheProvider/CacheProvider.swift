//
//  CacheProvider.swift
//  ITBOOKS
//
//  Created by Sanggeon Park on 2021/05/09.
//

import Foundation

typealias CacheSaveOperationCompletion = (_ error: Error?) -> Void
typealias CacheLoadOperationCompletion = (_ data: Data?, _ error: Error?) -> Void

class CacheProvider {
    enum CacheType: String {
        case image
        case document
        case raw
    }

    private let cachePathURL: URL

    // Using queue : cannot read, delete and save at the same time.
    private let operationQueue: OperationQueue

    init() {
        cachePathURL = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask)[0]
        operationQueue = OperationQueue()
        operationQueue.maxConcurrentOperationCount = 1
    }

    func saveCache(data: Data,
                   for identifier: String, type: CacheType = .image,
                   _ completion: @escaping CacheSaveOperationCompletion) {
        let targetURL = self.targetURL(identifier, type)
        operationQueue.addOperation {
            do {
                try data.write(to: targetURL, options: .atomicWrite)
                completion(nil)
            } catch {
                completion(error)
            }
        }
    }

    func loadCacheData(with identifier: String,
                       type: CacheType = .image,
                       _ completion: @escaping CacheLoadOperationCompletion) {
        let targetURL = self.targetURL(identifier, type)
        operationQueue.addOperation {
            do {
                let data = try Data(contentsOf: targetURL)
                completion(data, nil)
            } catch {
                completion(nil, error)
            }
        }
    }

    func deleteCacheData(with identifier: String,
                         type: CacheType,
                         _ completion: @escaping CacheSaveOperationCompletion) {
        let targetURL = self.targetURL(identifier, type)
        operationQueue.addOperation {
            do {
                try FileManager.default.removeItem(at: targetURL)
                completion(nil)
            } catch {
                completion(error)
            }
        }
    }

    private func targetURL(_ identifier: String, _ type: CacheType) -> URL {
        cachePathURL.appendingPathComponent(identifier).appendingPathExtension(type.rawValue)
    }
}
