//
//  NetworkProvider.swift
//  ITBOOKS
//
//  Created by Sanggeon Park on 2021/05/09.
//

import Foundation

typealias SearchRequestCompletionHandler       = (_ error: Error?, _ bookList: BookListJSONData?) -> Void
typealias BookDetailRequestCompletionHandler   = (_ error: Error?, _ bookDetail: BookDetailJSONData?) -> Void
typealias DataRequestCompletionHandler         = (_ error: Error?, _ data: Data?) -> Void


// TODO: return task and make the request cancelable
class NetworkProvider {
    
    private var urlSession: URLSession
    private var searchBaseURL: URL
    private var bookBaseURL: URL
    
    init() {
        self.searchBaseURL = URL(string: "https://api.itbook.store/1.0/search")!
        self.bookBaseURL = URL(string: "https://api.itbook.store/1.0/books")!
        urlSession = URLSession.shared
    }

    func searchBooksWith(keyword: String,
                     _ page: Int? = nil,
                     _ completion: @escaping SearchRequestCompletionHandler) {
        var requestURL = searchBaseURL.appendingPathComponent(keyword)
        if let pageNumber = page {
            requestURL = requestURL.appendingPathComponent(String(pageNumber))
        }
        let urlRequest = URLRequest(url: requestURL)
        let task = urlSession.dataTask(with: urlRequest) { data, response, error in
            NetworkProvider.handleJSONResponse(data, response, error, completion)
        }
        task.resume()
    }

    func bookDetailWith(isbn13: String, _ completion: @escaping BookDetailRequestCompletionHandler) {
        let requestURL = bookBaseURL.appendingPathComponent(isbn13)
        let urlRequest = URLRequest(url: requestURL)
        let task = urlSession.dataTask(with: urlRequest) { data, response, error in
            NetworkProvider.handleJSONResponse(data, response, error, completion)
        }
        task.resume()
    }

    func downloadDataFrom(url: URL, _ completion: @escaping DataRequestCompletionHandler) {
        let urlRequest = URLRequest(url: url)
        let task = urlSession.dataTask(with: urlRequest) { data, response, error in
            NetworkProvider.handleDataResponse(data, response, error, completion)
        }
        task.resume()
    }

}

// MARK: Private class functions
extension NetworkProvider {

    private class func handleError<T>(_ data: Data?, _ response: URLResponse?, _ error: Error?,
                                _ completion: @escaping (_ error: Error?, _ result: T?) -> Void) -> Data? {
        if let requestError = error {
            // TODO: Handle network request Error
            completion(requestError, nil)
            return nil
        }

        guard let httpResponse = response as? HTTPURLResponse else {
            // TODO: Handle Unkown Response Error
            completion(NSError(domain: "ResponseError", code: -1, userInfo: nil), nil)
            return nil
        }

        // TODO: define HTTP response code as Enum and handle it.
        guard (200...299).contains(httpResponse.statusCode) else {
            // TODO: Handle Server Response Error
            completion(NSError(domain: "ResponseError", code: -1, userInfo: ["errorCode": httpResponse.statusCode]), nil)
            return nil
        }

        guard let returnValue = data else {
            // TODO : Handle null data error
            completion(NSError(domain: "ResponseError", code: -1, userInfo: nil), nil)
            return nil
        }

        return returnValue
    }

    private class func handleJSONResponse<T>(_ data: Data?,
                                             _ response: URLResponse?,
                                             _ error: Error?,
                                             _ completion: @escaping (_ error: Error?, _ result: T?) -> Void)
    where T: Codable {
        guard let jsonData = handleError(data, response, error, completion) else {
            return
        }

        let decoder = JSONDecoder()

        do {
            let bookListData = try decoder.decode(T.self, from: jsonData)
            completion(nil, bookListData)
        } catch {
            // TODO : Handle JSON Decode Error
            completion(error, nil)
        }
    }

    private class func handleDataResponse(_ data: Data?, _ response: URLResponse?, _ error: Error?,
                                          _ completion: @escaping DataRequestCompletionHandler) {
        guard let responseData = handleError(data, response, error, completion) else {
            return
        }
        completion(nil, responseData)
    }
}
