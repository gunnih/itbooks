//
//  BookDetailJSONData.swift
//  ITBOOKS
//
//  Created by Sanggeon Park on 2021/05/09.
//

import Foundation

struct BookDetailJSONData: BaseBookJSONData {

    // MARK: BaseBookJSONData properties
    let title: String
    let subtitle: String
    let price: String
    let image: String
    let isbn13: String
    let url: String

    // MARK: BookDetailJSONData properties
    let error: String
    let authors: String
    let publisher: String
    let language: String?
    let isbn10: String
    let pages: String
    let year: String
    let rating: String
    let desc: String
    let pdf: Dictionary<String, String>?
}
