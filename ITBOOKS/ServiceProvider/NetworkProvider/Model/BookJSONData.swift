//
//  BookJSONData.swift
//  ITBOOKS
//
//  Created by Sanggeon Park on 2021/05/09.
//

import Foundation

struct BookJSONData: BaseBookJSONData {

    // MARK: BaseBookJSONData properties
    let title: String
    let subtitle: String
    let price: String
    let image: String
    let isbn13: String
    let url: String
}
