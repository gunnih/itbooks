//
//  BookListJSONData.swift
//  ITBOOKS
//
//  Created by Sanggeon Park on 2021/05/09.
//

import Foundation

struct BookListJSONData: Codable {
    let error: String
    let total: String
    let page: String?
    let books: Array<BookJSONData>
}
