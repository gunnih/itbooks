//
//  BaseBookJSONData.swift
//  ITBOOKS
//
//  Created by Sanggeon Park on 2021/05/09.
//

import Foundation

protocol BaseBookJSONData: Codable {
    var title: String { get }
    var subtitle: String { get }
    var price: String { get }
    var image: String { get }
    var isbn13: String { get }
    var url: String { get }
}
