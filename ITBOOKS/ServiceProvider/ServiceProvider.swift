//
//  ServiceProvider.swift
//  ITBOOKS
//
//  Created by Sanggeon Park on 2021/05/09.
//

import Foundation

final class ServiceProvider {
    let databaseProvider = DataBaseProvider()
    let cacheProvider = CacheProvider()
    let networkProvider = NetworkProvider()
    
    // TODO: cancel previous request if other keyword searching starts
    func booksFor(_ keyword: String,
                  page: Int? = 0,
                  _ completion: @escaping (Int?, [BookData]?, Error?) -> Void) {
        networkProvider.searchBooksWith(keyword: keyword, page) { [weak self] (error, listJSON) in
            guard let strongSelf = self, let list = listJSON, error == nil else {
                DispatchQueue.main.async {
                    completion(nil, nil, error)
                }
                return
            }

            // Store received BookData
            strongSelf.databaseProvider.addOrUpdateBooks(list.books) { error in
                // TODO: Handle Error
            }
            DispatchQueue.main.async {
                completion(Int(list.page ?? ""), strongSelf.convert(list.books), nil)
            }
        }
    }
    
    func bookDetailWith(isbn13: String,
                        _ completion: @escaping (BookData?, Error?) -> Void) {
        networkProvider.bookDetailWith(isbn13: isbn13) { [weak self] (error, bookJSON) in
            guard let strongSelf = self,
                  error == nil,
                  let bookDetail = bookJSON else {
                DispatchQueue.main.async {
                    completion(nil, error)
                }
                return
            }

            strongSelf.databaseProvider.addOrUpdateBook(bookDetail) { error in
                guard error == nil else {
                    // TODO: Handle Error
                    fatalError("\(error!)")
                }
            }
            
            strongSelf.databaseProvider.bookWith(isbn13: isbn13) { [weak self] (book, error) in
                guard let strongSelf = self,
                      error == nil,
                      let bookDetail = book else {
                    DispatchQueue.main.async {
                        completion(nil, error)
                    }
                    return
                }
                DispatchQueue.main.async {
                    completion(strongSelf.convert(bookDetail), nil)
                }
            }
        }
    }
    
    func imageDataFor(book: BookData,
                      _ completion: @escaping (_ isbn13: String, Data?, Error?) -> Void) {
        let isbn13 = book.isbn13
        let imageURLString = book.image
        cacheProvider.loadCacheData(with: book.isbn13) { [weak self] (data, error) in
            guard let imageData = data else {
                guard let imageURL = URL(string: imageURLString) else {
                    let error = NSError(domain: "ITBOOKS",
                                        code: -2,
                                        userInfo: ["description": "Failed to create image URL"])
                    DispatchQueue.main.async {
                        completion(isbn13, nil, error)
                    }
                    return
                }

                self?.networkProvider.downloadDataFrom(url: imageURL) { error, data in
                    guard let imageData = data, error == nil else {
                        DispatchQueue.main.async {
                            completion(isbn13, nil, error)
                        }
                        return
                    }
                    self?.cacheProvider.saveCache(data: imageData, for: isbn13) { error in
                        // TODO: handle error
                    }
                    DispatchQueue.main.async {
                        completion(isbn13, data, nil)
                    }
                }
                return
            }
            
            DispatchQueue.main.async {
                completion(isbn13, imageData, nil)
            }
        }
    }
    
    func addNote(_ note : String, book: BookData, _ completion: @escaping (Error?) -> Void) {
        databaseProvider.updateComment(isbn13: book.isbn13, note: note) { error in
            DispatchQueue.main.async {
                completion(error)
            }
        }
    }
}

private extension ServiceProvider {
    func convert(_ listJSON: [BookJSONData]) -> [BookData] {
        var convertedBooks = [BookData]()
        for jsonData in listJSON {
            let convertedBook = BookData(title: jsonData.title,
                                         subtitle: jsonData.subtitle,
                                         price: jsonData.price,
                                         image: jsonData.image,
                                         isbn13: jsonData.isbn13,
                                         url: jsonData.url,
                                         authors: nil, publisher: nil,
                                         isbn10: nil, pages: nil, year: nil,
                                         rating: nil, desc: nil, language: nil,
                                         PDFDataArray: nil,
                                         note: nil)
            convertedBooks.append(convertedBook)
        }
        return convertedBooks
    }

    func convert(_ book: Book) -> BookData {
        var convertedPDFs = [SectionPDFData]()

        for pdfObject in book.pdf?.array ?? [] {

            guard let pdf = pdfObject as? SectionPDF,
                  let isbn13 = pdf.isbn13,
                  let url = pdf.url,
                  let title = pdf.sectionTitle else {
                continue
            }

            let convertedPDF = SectionPDFData(isbn13: isbn13, url: url, title: title)
            convertedPDFs.append(convertedPDF)
        }

        let convertedBook = BookData(title: book.title ?? "",
                                     subtitle: book.subtitle ?? "",
                                     price: book.price ?? "",
                                     image: book.image ?? "",
                                     isbn13: book.isbn13 ?? "",
                                     url: book.url ?? "",
                                     authors: book.authors ?? "",
                                     publisher: book.publisher ?? "",
                                     isbn10: book.isbn10 ?? "",
                                     pages: book.pages ?? "",
                                     year: book.year ?? "",
                                     rating: book.rating,
                                     desc: book.desc,
                                     language: book.language ?? "",
                                     PDFDataArray: convertedPDFs,
                                     note: book.comment?.comment)
        return convertedBook
    }
    
    func convert(json book: BookDetailJSONData) -> BookData? {
        var convertedPDFs = [SectionPDFData]()
        if let dictionary = book.pdf {
            for title in dictionary.keys {
                guard let url = dictionary[title] else {
                    continue
                }

                let convertedPDF = SectionPDFData(isbn13: book.isbn13, url: url, title: title)
                convertedPDFs.append(convertedPDF)
            }
        }

        let convertedBook = BookData(title: book.title,
                                     subtitle: book.subtitle,
                                     price: book.price,
                                     image: book.image,
                                     isbn13: book.isbn13,
                                     url: book.url,
                                     authors: book.authors,
                                     publisher: book.publisher,
                                     isbn10: book.isbn10,
                                     pages: book.pages,
                                     year: book.year,
                                     rating: book.rating,
                                     desc: book.desc,
                                     language: book.language ?? "",
                                     PDFDataArray: convertedPDFs,
                                     note: nil)
        return convertedBook
    }
}
