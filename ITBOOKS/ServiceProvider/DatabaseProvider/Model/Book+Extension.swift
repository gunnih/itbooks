//
//  Book+Extension.swift
//  ITBOOKS
//
//  Created by Sanggeon Park on 2021/05/09.
//

import Foundation
import CoreData

// MARK: Helper
extension Book {
    func update(from json: BaseBookJSONData, managedObjectContext: NSManagedObjectContext) {
        self.title = json.title
        self.subtitle = json.subtitle
        self.price = json.price
        self.image = json.image
        self.isbn13 = json.isbn13
        self.url = json.url

        if let detailData = json as? BookDetailJSONData {
            self.authors = detailData.authors
            self.publisher = detailData.publisher
            self.language = detailData.language
            self.isbn10 = detailData.isbn10
            self.pages = detailData.pages
            self.year = detailData.year
            self.rating = detailData.rating
            self.desc = detailData.desc

            if let pdfDictionary = detailData.pdf {
                updatePDFInformation(pdfDictionary, managedObjectContext: managedObjectContext)
            }
        }
    }

    func updatePDFInformation(_ pdfDictionary: Dictionary<String, String>,
                              managedObjectContext: NSManagedObjectContext) {
        for pdfObject in self.pdf?.array ?? [] {
            guard let pdf = pdfObject as? SectionPDF else {
                continue
            }
            managedObjectContext.delete(pdf)
        }
        self.pdf = nil
        let tmpPDFs = NSMutableOrderedSet()

        let pdfEntityName = String(describing: SectionPDF.self)
        guard let entityDescription =
                NSEntityDescription.entity(forEntityName: pdfEntityName, in: managedObjectContext) else {
            fatalError("Failed to create \(pdfEntityName)")
        }

        for title in pdfDictionary.keys {
            let pdf = SectionPDF(entity: entityDescription, insertInto: managedObjectContext)
            pdf.sectionTitle = title
            pdf.url = pdfDictionary[title]
            pdf.isbn13 = self.isbn13
            tmpPDFs.add(pdf)
        }
        self.pdf = tmpPDFs
    }

    func addOrUpdateComment(_ comment: String, managedObjectContext: NSManagedObjectContext) {
        if self.comment == nil {
            let commentEntityName = String(describing: UserComment.self)
            guard let entityDescription =
                    NSEntityDescription.entity(forEntityName: commentEntityName, in: managedObjectContext) else {
                fatalError("Failed to create \(commentEntityName)")
            }
            self.comment = UserComment(entity: entityDescription, insertInto: managedObjectContext)
        }
        self.comment?.comment = comment
        self.comment?.isbn13 = self.isbn13
    }

    func removeUserComment(from managedObjectContext: NSManagedObjectContext) {
        if let userComment = self.comment {
            managedObjectContext.delete(userComment)
        }
        self.comment = nil
    }
}
