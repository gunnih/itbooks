//
//  DataBaseProvider+Service.swift
//  ITBOOKS
//
//  Created by Sanggeon Park on 2021/05/09.
//

import Foundation
import CoreData

extension DataBaseProvider {

    func addOrUpdateBooks(_ bookData: [BaseBookJSONData], _ completion: @escaping (Error?) -> Void) {
        let databaseContext = self.databaseContext
        databaseContext.perform {
            for tmpBook in bookData {
                self.updateBook(tmpBook, databaseContext)

                do {
                    try databaseContext.save()
                    completion(nil)
                } catch {
                    completion(error)
                }
            }
        }
    }
    
    func addOrUpdateBook(_ bookData: BaseBookJSONData, _ completion: @escaping (Error?) -> Void) {
        let databaseContext = self.databaseContext
        databaseContext.perform {
            self.updateBook(bookData, databaseContext)

            do {
                try databaseContext.save()
                completion(nil)
            } catch {
                completion(error)
            }
        }
    }
    
    private func updateBook(_ bookData: BaseBookJSONData, _ databaseContext: NSManagedObjectContext) {

        bookWith(isbn13: bookData.isbn13) { tmpEntity, error in
            var tmpEntity = tmpEntity
            if tmpEntity == nil {
                guard let entityDescription =
                        NSEntityDescription.entity(forEntityName: "Book", in: databaseContext) else {
                    // TODO: define Database Error Domain, error code and description
                    fatalError("Critical error while creating Book Entity Description.")
                }
                
                tmpEntity = Book(entity: entityDescription, insertInto: databaseContext)
            }
            
            guard let bookEnity = tmpEntity else {
                // TODO: define Database Error Domain, error code and description
                fatalError("Cannot fetch and create Book Entity with isbn13: \(bookData.isbn13)")
            }
            
            bookEnity.update(from: bookData, managedObjectContext: databaseContext)
            }
    }
    
    func updateComment(isbn13: String, note: String, _ completion: @escaping (Error?) -> Void) {
        let databaseContext = self.databaseContext
        databaseContext.perform {
            self.bookWith(isbn13: isbn13) { book, error in
                guard error == nil else {
                    completion(error!)
                    return
                }

                book?.addOrUpdateComment(note, managedObjectContext: databaseContext)
                completion(nil)
            }

            do {
                try databaseContext.save()
                completion(nil)
            } catch {
                completion(error)
            }
        }
    }

    func bookWith(isbn13: String, completion: @escaping (Book?, Error?) -> Void) {
        let databaseContext = self.databaseContext
        let request = Book.fetchRequest()
        request.predicate = NSPredicate(format: "isbn13 == %@", isbn13)
        do {
            let result = try databaseContext.fetch(request)
            completion(result.first, nil)
        } catch {
            completion(nil, error)
        }
    }

    func clean(_ completion: @escaping (Error?) -> Void) {
        let databaseContext = self.databaseContext
        let bookFetchRequest = Book.fetchRequest()
        databaseContext.perform {
            do {
                let result = try databaseContext.fetch(bookFetchRequest)
                for book in result {
                    book.removeUserComment(from: databaseContext)
                    databaseContext.delete(book)
                }
            } catch {
                // TODO: define Database Error Domain, error code and description
                fatalError("Critical error while deleting Book and its comment")
            }

            do {
                try databaseContext.save()
                completion(nil)
            } catch {
                completion(error)
            }
        }
    }
}
