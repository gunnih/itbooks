//
//  DataBaseProviderTests.swift
//  ITBOOKSTests
//
//  Created by Sanggeon Park on 2021/05/09.
//

import XCTest
@testable import ITBOOKS

class DataBaseProviderTests: XCTestCase {
    let databaseProvider = DataBaseProvider()
    let isbn13 = "9781617294136"

    func test00DatabaseContext() throws {
        XCTAssertNotNil(databaseProvider.databaseContext)
        let expectation = expectation(description: "Waiting for fetching book")
        databaseProvider.bookWith(isbn13: "dummy") { book, error in
            XCTAssertNil(book, "dummy isbn13 should return nil object.")
            XCTAssertNil(error, "No error should be occurred")
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 1.0)

        // Clean DB
        let cleaningExpectation = self.expectation(description: "Waiting for cleaning database")
        databaseProvider.clean() { error in
            cleaningExpectation.fulfill()
        }
        wait(for: [cleaningExpectation], timeout: 1.0)
    }

    func test01AddBookSummary() throws {
        XCTAssertNotNil(databaseProvider.databaseContext)

        let JSONString = """
            {
                "title": "Securing DevOps",
                "subtitle":"Security in the Cloud",
                "isbn13":"\(isbn13)",
                "price":"$26.98",
                "image":"https://itbook.store/img/books/\(isbn13).png",
                "url":"https://itbook.store/books/\(isbn13)"
            }
            """
        guard let bookJSONData = JSONString.data(using: .utf8) else {
            XCTAssert(false, "Failed to create test data")
            return
        }

        let addOrUpdateExpectation = self.expectation(description: "AddOrUpdateExpectation")
        do {
            let bookData = try JSONDecoder().decode(BookJSONData.self, from: bookJSONData)
            databaseProvider.addOrUpdateBooks([bookData]) { error in
                XCTAssertNil(error, "Error should be nil")
                addOrUpdateExpectation.fulfill()
            }
        } catch {
            XCTAssert(false, "failed to add new Book data into database \(error)")
            addOrUpdateExpectation.fulfill()
        }

        wait(for: [addOrUpdateExpectation], timeout: 1.0)

        let expectation = expectation(description: "Waiting for fetching book")
        databaseProvider.bookWith(isbn13: isbn13) { book, error in
            XCTAssertNotNil(book, "Cannot fetch book entity with isbn13: \(self.isbn13)")
            XCTAssert(book?.desc == nil || book?.desc?.count == 0)
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 1.0)
    }

    func test02UpdateBookDetail() throws {
        let JSONString = """
            {
                "error": "0",
                "title": "Securing DevOps",
                "subtitle": "Security in the Cloud",
                "authors": "Julien Vehent",
                "publisher": "Manning",
                "isbn10": "1617294136",
                "isbn13": "\(isbn13)",
                "pages": "384",
                "year": "2018",
                "rating": "5",
                "desc": "An application running in the cloud can benefit from incredible efficiencies, but they come with unique security threats too. A DevOps team's highest priority is understanding those risks and hardening the system against them.Securing DevOps teaches you the essential techniques to secure your cloud ...",
                "price": "$26.98",
                "image": "https://itbook.store/img/books/\(isbn13).png",
                "url": "https://itbook.store/books/\(isbn13)",
                "pdf": {
                          "Chapter 2": "https://itbook.store/files/\(isbn13)/chapter2.pdf",
                          "Chapter 5": "https://itbook.store/files/\(isbn13)/chapter5.pdf"
                       }
            }
            """
        guard let bookJSONData = JSONString.data(using: .utf8) else {
            XCTAssert(false, "Failed to create test data")
            return
        }

        let expectation = expectation(description: "Waiting for updating book")
        do {
            let bookData = try JSONDecoder().decode(BookDetailJSONData.self, from: bookJSONData)
            databaseProvider.addOrUpdateBooks([bookData]) { error in
                XCTAssertNil(error, "Error should be nil")
                expectation.fulfill()
            }
        } catch {
            XCTAssert(false, "failed to add new Book data into database \(error)")
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 1.0)

        let fetchExpectation = self.expectation(description: "Waiting for fetching book")

        databaseProvider.bookWith(isbn13: isbn13) { book, error in
            XCTAssertNil(error, "Error should be nil")
            XCTAssertNotNil(book, "Cannot fetch book entity with isbn13: \(self.isbn13)")
            XCTAssertNotNil(book?.desc, "Description should not be nil")
            XCTAssert(book?.desc?.count != 0, "Description should not be zero length string")
            XCTAssert(book?.pdf?.count ?? 0 == 2)
            fetchExpectation.fulfill()
        }

        wait(for: [fetchExpectation], timeout: 1.0)
    }

    func test10AddComment() throws {
        XCTAssertNotNil(databaseProvider.databaseContext)
        let expectation = self.expectation(description: "Waiting for fetching book")
        databaseProvider.bookWith(isbn13: isbn13) { book, error in
            XCTAssertNotNil(book, "Cannot fetch book entity with isbn13: \(self.isbn13)")
            book?.addOrUpdateComment("Added Comment", managedObjectContext: self.databaseProvider.databaseContext)
            self.databaseProvider.saveContext()
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 1.0)

        let fetchExpectation = self.expectation(description: "Waiting for fetching book")
        databaseProvider.bookWith(isbn13: isbn13) { book, error in
            XCTAssertNotNil(book, "Cannot fetch book entity with isbn13: \(self.isbn13)")
            XCTAssertNotNil(book?.comment)
            fetchExpectation.fulfill()
        }
        wait(for: [fetchExpectation], timeout: 1.0)
    }

    func test11RemoveComment() throws {
        XCTAssertNotNil(databaseProvider.databaseContext)
        let expectation = self.expectation(description: "Waiting for fetching book")
        databaseProvider.bookWith(isbn13: isbn13) { book, error in
            XCTAssertNotNil(book, "Cannot fetch book entity with isbn13: \(self.isbn13)")
            XCTAssertNotNil(book?.comment, "Comment should not be nil")
            book?.removeUserComment(from: self.databaseProvider.databaseContext)
            self.databaseProvider.saveContext()
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 1.0)

        let fetchExpectation = self.expectation(description: "Waiting for fetching book")
        databaseProvider.bookWith(isbn13: isbn13) { book, error in
            XCTAssertNotNil(book, "Cannot fetch book entity with isbn13: \(self.isbn13)")
            XCTAssertNil(book?.comment, "Comment should be nil")
            fetchExpectation.fulfill()
        }
        wait(for: [fetchExpectation], timeout: 1.0)
    }

    func test20RemoveBook() throws {
        XCTAssertNotNil(databaseProvider.databaseContext)
    }
}
