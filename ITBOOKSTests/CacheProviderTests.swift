//
//  CacheProviderTests.swift
//  ITBOOKSTests
//
//  Created by Sanggeon Park on 2021/05/09.
//

import XCTest
@testable import ITBOOKS

class CacheProviderTests: XCTestCase {
    let identifier = "CacheProviderTestIdentifier"
    let type = CacheProvider.CacheType.raw
    let unkownIdentifier = "unknown_identifier"
    let wrongType = CacheProvider.CacheType.image

    func test01SaveCache() throws {
        let cacheProvider = CacheProvider()
        let expectation = expectation(description: "Waiting for saving cache")
        guard let cafe = identifier.data(using: .utf8) else {
            XCTAssert(false, "Failed to create test data")
            return
        }
        cacheProvider.saveCache(data: cafe, for: identifier, type: type) { error in
            XCTAssertNil(error)
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 1.0)
    }

    func test10LoadCache() throws {
        let cacheProvider = CacheProvider()
        let expectation = expectation(description: "Waiting for loading cache")
        cacheProvider.loadCacheData(with: identifier, type: type) { data, error in
            XCTAssertNil(error)
            guard let data = data else {
                XCTAssert(false, "Failed to load cached data")
                return
            }
            let decodedString = String(decoding: data, as: UTF8.self)
            XCTAssertEqual(self.identifier, decodedString, "Cache is different from expected identifier")
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 1.0)
    }

    func test11LoadCacheWithUnkownIdentifier() throws {
        let cacheProvider = CacheProvider()
        let expectation = expectation(description: "Waiting for loading cache")

        cacheProvider.loadCacheData(with: unkownIdentifier, type: type) { data, error in
            XCTAssertNil(data, "The data must be nil")
            XCTAssertNotNil(error, "An error must exist")
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 1.0)
    }

    func test12LoadCacheWithWrongType() throws {
        let cacheProvider = CacheProvider()
        let expectation = expectation(description: "Waiting for loading cache")

        cacheProvider.loadCacheData(with: identifier, type: wrongType) { data, error in
            XCTAssertNil(data, "The data must be nil")
            XCTAssertNotNil(error, "An error must exist")
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 1.0)
    }

    func test20DeleteCacheWithUnkownIdentifier() throws {
        let cacheProvider = CacheProvider()
        let expectation = expectation(description: "Waiting for loading cache")

        cacheProvider.deleteCacheData(with: unkownIdentifier, type: type) { error in
            XCTAssertNotNil(error, "An error must exist")
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 1.0)
    }

    func test21DeleteCacheWithWrongType() throws {
        let cacheProvider = CacheProvider()
        let expectation = expectation(description: "Waiting for loading cache")

        cacheProvider.deleteCacheData(with: identifier, type: wrongType) { error in
            XCTAssertNotNil(error, "An error must exist")
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 1.0)
    }

    func test21DeleteCacheWithUnkownIdentifierAndWrongType() throws {
        let cacheProvider = CacheProvider()
        let expectation = expectation(description: "Waiting for deleting cache")

        cacheProvider.deleteCacheData(with: unkownIdentifier, type: wrongType) { error in
            XCTAssertNotNil(error, "An error must exist")
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 1.0)
    }

    func test22DeleteCache() throws {
        let cacheProvider = CacheProvider()
        let expectation = expectation(description: "Waiting for deleting cache")

        cacheProvider.deleteCacheData(with: identifier, type: type) { error in
            XCTAssertNil(error, "The error must be nil")
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 1.0)
    }
}
