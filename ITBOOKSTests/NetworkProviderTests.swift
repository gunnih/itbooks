//
//  NetworkProviderTests.swift
//  ITBOOKSTests
//
//  Created by Sanggeon Park on 2021/05/09.
//

import XCTest
@testable import ITBOOKS

class NetworkProviderTests: XCTestCase {
    let bookISBN13 = "9781849517324"

    func testSearch() throws {
        let networkProvider = NetworkProvider()
        let expectation = expectation(description: "Waiting for book list response")
        networkProvider.searchBooksWith(keyword: "swift", 1) { error, bookListData in
            XCTAssertNil(error)
            XCTAssertNotNil(bookListData)
            XCTAssertNotNil(bookListData?.books)
            XCTAssertEqual(bookListData?.error, "0")
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 5.0)
    }

    func testBookDetail() throws {
        let networkProvider = NetworkProvider()
        let expectation = expectation(description: "Waiting for book detail response")
        networkProvider.bookDetailWith(isbn13: bookISBN13) { error, bookDetailData in
            XCTAssertNil(error)
            XCTAssertNotNil(bookDetailData)
            XCTAssertEqual(bookDetailData?.error, "0")
            XCTAssert(bookDetailData?.isbn13 == self.bookISBN13)

            XCTAssertNotNil(bookDetailData?.title)
            XCTAssertNotNil(bookDetailData?.subtitle)
            XCTAssertNotNil(bookDetailData?.price)
            XCTAssertNotNil(bookDetailData?.isbn13)
            XCTAssertNotNil(bookDetailData?.image)
            XCTAssertNotNil(bookDetailData?.url)

            XCTAssertNotNil(bookDetailData?.authors)
            XCTAssertNotNil(bookDetailData?.publisher)
            XCTAssertNotNil(bookDetailData?.language)
            XCTAssertNotNil(bookDetailData?.isbn10)
            XCTAssertNotNil(bookDetailData?.pages)
            XCTAssertNotNil(bookDetailData?.year)
            XCTAssertNotNil(bookDetailData?.desc)
            XCTAssertNotNil(bookDetailData?.rating)
            
            // pdf information is optional
            if let pdf = bookDetailData?.pdf {
                XCTAssertNotNil(pdf.keys.count > 0)
                for key in pdf.keys {
                    XCTAssertNotNil(pdf[key])
                    XCTAssert(pdf[key]?.count ?? 0 > 0)
                }
            }

            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 5.0)
    }

    func testDownloadImage() throws {
        let networkProvider = NetworkProvider()
        let expectation = expectation(description: "Waiting for image data response")
        guard let imageURL = URL(string: "https://itbook.store/img/books/\(bookISBN13).png") else {
            XCTAssert(false, "Failed to create image URL.")
            return
        }
        networkProvider.downloadDataFrom(url: imageURL) { error, imageData in
            XCTAssertNil(error)
            XCTAssertNotNil(imageData)
            if let pngData = imageData {
                XCTAssertNotNil(UIImage(data: pngData))
            }
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 5.0)
    }
}
